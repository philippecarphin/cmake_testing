#include "myapi.h"

int test_add_1(){
  int result = add(8,9);
  int expected = 17;
  return result == expected;
}

int test_add_2(){
  int result = add(9,9);
  int expected = 18;
  return result == expected;
}

int main(void){
  if(!test_add_1()){
    return 1;
  }
  if(!test_add_2()){
    return 1;
  }
  return 0;
}
