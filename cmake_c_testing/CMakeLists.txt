cmake_minimum_required(VERSION 3.10)

project(c_testing_demo C)

#### Normal non-testing stuff ####
# We have a library and an executable that links with it.
add_library(myapi_c myapi.c)

add_executable(main_c main.c)
target_link_libraries(main_c myapi_c)

#### Testing stuff ####
# we will test the executable by just trying to run it and we will test the
# library by writing short source code files with a main that runs tests and
# exits with non-zero status if one of them fails.  CTest will run these
# executables for us and report the results.

include(CTest)
add_custom_target(check COMMAND ${CMAKE_CTEST_COMMAND})

# This test is just trying to run the executable
add_test(
  NAME test_exec
  COMMAND main_c
)
add_dependencies(check main_c)

# Creates a *NORMAL* CMake target ${test_name} and adds it the dependencies of the *check*
# target.
# test_name : Name of the created target and also name of test added with
#             add_test()
# test_source : Source file(s) for compiling an executable
# test_dep    : Targets that this executable depends on
# test_link   : Passed to target_link_libraries
#
# We should provide such macros but also people should be OK creating their own.
#
# For example, SPOOKI uses Boost Unit Test framework, so their add_spooki_test
# has the defines, include directories, link libraries for boost unit test right
# in the macro.
macro(add_phil_test test_name test_source test_dep test_link)
    add_executable(${test_name} ${test_source})
    add_test(
        NAME ${test_name}
        COMMAND ${test_name}
    )
    add_dependencies(${test_name} "${test_dep}")
    target_link_libraries(${test_name} "${test_link}")
    add_dependencies(check ${test_name})
endmacro()

add_phil_test(add_test_c add_test.c myapi_c myapi_c)
add_phil_test(sub_test_c sub_test.c myapi_c myapi_c)

message(STATUS "CMAKE_C_FLAGS_DEBUG = ${CMAKE_C_FLAGS_DEBUG}")
message(STATUS "CMAKE_C_FLAGS_RELEASE = ${CMAKE_C_FLAGS_RELEASE}")

# Note: Instead of requiring things to be added to the dependencies of the
# 'check' target, we could have done
#
#       add_custom_target(check COMMAND make && make test)
#
# which I think is completely fine and readable: It's a target that ensures that
# 'make all' is done before 'make test' because, sadly, running 'make test'
# doesn't cause anything to get built or rebuilt.
