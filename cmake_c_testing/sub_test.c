#include "myapi.h"

int test_sub_1(){
  int result = sub(8,9);
  int expected = -1;
  return result == expected;
}

int test_sub_2(){
  int result = sub(9,9);
  int expected = 0;
  return result == expected;
}

int main(void){
  if(!test_sub_1()){
    return 1;
  }
  if(!test_sub_2()){
    return 1;
  }
  return 0;
}
