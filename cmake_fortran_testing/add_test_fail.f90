program addNumbers
! This simple program adds two numbers
   implicit none

! Type declarations
   real :: a, b, result, expect

! Executable statements
   a = 12.0
   b = 15.0
   result = a + b
   expect = 25.0
   print *, 'The total is ', result
   if (result - expect > 1) then
     call exit(-1)
   end if
end program addNumbers
