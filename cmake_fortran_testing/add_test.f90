program addNumbers
! This simple program adds two numbers
   implicit none

! Type declarations
   real :: a, b, result

! Executable statements
   a = 12.0
   b = 15.0
   result = a + b
   print *, 'The total is ', result
   if (result - 27.0 > 1) then
     call exit(-1)
   end if
end program addNumbers
