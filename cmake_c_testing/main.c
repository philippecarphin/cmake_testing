#include <stdio.h>
#include "myapi.h"

#ifndef NDEBUG
#define dbgmsg printf("Debug message (inside #ifndef NDEBUG)\n")
#else
#define dbgmsg
#endif

int main(int argc, char **argv){
  printf("result = %d\n", add(argc, 8));
  dbgmsg;
  return 0;
}
